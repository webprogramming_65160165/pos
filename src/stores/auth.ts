import { ref, computed } from 'vue'
import { defineStore } from 'pinia'
import type { User } from '@/types/User'

export const useAuthStore = defineStore('auth', () => {
    const CurrentUser = ref<User>({
        id: 1,
        email: 'GGG123@gmail.com',
        password: 'Pass@1234',
        fullName: 'สมชาย งานดี',
        gender: 'male',
        roles: ['user']
    })


    return { CurrentUser }
})

