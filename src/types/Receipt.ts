import type { Member } from "./Member";
import type { User } from "./User";
import type { ReciptItem } from "./ReceiptItem";
type Receipt = {
    id: number;
    createdDate: Date;
    totalBefore: number;
    memberDiscount: number;
    total: number;
    receivedAmount: number;
    change: number;
    paymentType: string;
    userId: number;
    user?: User;
    memberId: number;
    member?: Member;
    receiptItems?: ReciptItem[]
}
export type { Receipt }