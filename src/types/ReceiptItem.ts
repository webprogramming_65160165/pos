import type { Product } from "./Product"

type ReciptItem = {
    id: number
    name: string
    price: number
    unit: number
    productId: number
    product?: Product
}

export { type ReciptItem }